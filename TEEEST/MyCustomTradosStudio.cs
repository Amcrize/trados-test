﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Sdl.Desktop.IntegrationApi;
using Sdl.Desktop.IntegrationApi.Extensions;
using Sdl.Desktop.IntegrationApi.Extensions.Internal;
using Sdl.FileTypeSupport.Framework.BilingualApi;
using Sdl.TranslationStudioAutomation.IntegrationApi;
using Sdl.TranslationStudioAutomation.IntegrationApi.Extensions;
using Sdl.TranslationStudioAutomation.IntegrationApi.Presentation.DefaultLocations;

namespace TEEEST
{
	[RibbonGroup("Sdl.HelloWorld", Name = "")]
	[RibbonGroupLayout(LocationByType = typeof(TranslationStudioDefaultRibbonTabs.HomeRibbonTabLocation))]

	class BringBackTheButtonRibbon : AbstractRibbonGroup
	{


	}

	[Action("Sdl.HelloWorld", Name = "GCS plugin", Icon = "Icon", Description = "Press to chose from dropdown list")]
	[ActionLayout(typeof(BringBackTheButtonRibbon), 20, DisplayType.Large)]

	class BringBackTheButtonViewPartAction : AbstractAction
	{
		ComboBox combo;

        public override void Initialize()
        {
			combo = new ComboBox();
			combo.Items.Add("Hello World");

			this.combo.SelectedIndexChanged += new System.EventHandler(this.combo_SelectedIndexChanged);

		}

        protected override void Execute()
		{
			
			combo.DroppedDown = true;
		
		}

		public void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
			if(combo.SelectedIndex == 0)
            {
				MessageBox.Show("Hello World");
            }
        }
	}


	[ViewPartLayout(typeof(EditorController), Dock = DockType.Bottom)]
	class BringBackTheButtonPartController : AbstractViewPartController
	{
		protected override Control GetContentControl()
		{
			throw new NotImplementedException();
		}

		protected override void Initialize()
		{
			throw new NotImplementedException();
		}
	}
}


